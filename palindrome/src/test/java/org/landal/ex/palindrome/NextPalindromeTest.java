package org.landal.ex.palindrome;

import static org.junit.Assert.*;

import org.junit.Test;

public class NextPalindromeTest {
	
	private final NextPalindrome nextPalindrome = new NextPalindrome();

	@Test
	public void test_palindrome_1() {
		assertEquals("818", nextPalindrome.findNext("808"));
	}

	@Test
	public void test_palindrome_2() {
		assertEquals("2222", nextPalindrome.findNext("2133"));
	}

	@Test
	public void test_palindrome_3() {
		assertEquals("934439", nextPalindrome.findNext("934267"));
	}

	@Test
	public void test_palindrome_4() {
		assertEquals("933339", nextPalindrome.findNext("932467"));
	}

	@Test
	public void test_palindrome_5() {
		assertEquals("2882", nextPalindrome.findNext("2881"));
	}

//	@Test
//	public void test_palindrome_6() {
//		assertEquals("523526236747348343343843747632625325",
//				Palindrome.findNext("523526236747348343262362486125986592"));
//	}

	@Test
	public void test_palindrome_7() {
		assertEquals("13031", nextPalindrome.findNext("12921"));
	}

	@Test
	public void test_palindrome_8() {
		assertEquals("783387", nextPalindrome.findNext("783322"));
	}
	
	@Test
	public void test_palindrome_9() {
		assertEquals("125521", nextPalindrome.findNext("125322"));
	}

	@Test
	public void test_palindrome_10() {
		assertEquals("14587678541", nextPalindrome.findNext("14587678322"));
	}

	@Test
	public void test_palindrome_11() {
		assertEquals("714417", nextPalindrome.findNext("713322"));
	}
	
	@Test
	public void test_palindrome_12() {
		assertEquals("1235321", nextPalindrome.findNext("1234628"));
	}
		
	@Test
	public void test_palindrome_13() {
		assertEquals("94188088149", nextPalindrome.findNext("94187978322"));
	}
}
