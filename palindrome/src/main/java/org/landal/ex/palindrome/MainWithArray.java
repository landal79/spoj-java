package org.landal.ex.palindrome;

import java.util.Scanner;

public class MainWithArray {

	public static void main(String[] args) {

		try {
			Scanner scanner = new Scanner(System.in);

			int count = Integer.valueOf(scanner.nextLine());
			for (int i = 0; i < count; ++i) {
				System.out.println(PalindromeWithArray.findNext(scanner
						.nextLine()));
			}
			scanner.close();
		} catch (Exception e) {

		}

	}

}

class PalindromeWithArray {

	public static String findNext(String num) {
		return arrayToString(findNextInt(toIntArray(num)));
	}

	private static int[] toIntArray(String num) {
		
		if(num.length() == 1){
			return new int[]{new Integer(num)};
		}
		
		char[] cs = num.toCharArray();
		int i = 0;
		while (cs[i] == '0')
			++i;

		int[] result = new int[cs.length - i];
		for (int j = 0; j < (cs.length - i); ++j) {
			result[j] = new Integer(Character.toString(cs[j + i]));
		}
		return result;
	}

	private static String arrayToString(int[] word) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < word.length; i++) {
			sb.append(word[i]);
		}

		return sb.toString();
	}

	public static int[] findNextInt(final int[] word) {

		int size = word.length;
		if (size == 1) {
			return word;
		}

		if (areAll9s(word)) {
			int[] result = new int[word.length + 1];
			result[0] = 1;
			size = result.length - 1;
			result[size] = 1;
			for (int i = 1; i < size; i++)
				result[i] = 0;
			return result;
		}

		int middle = size / 2;

		if (size == 1) {
			return word;
		}

		if (size % 2 == 0) {
			int i = findIndexToCompare(word, middle, 0);
			if (word[middle + i] < word[middle - 1 - i]) {
				reflect(word, middle, 0);
				return word;
			} else {
				inc(word, middle - (i == 0 ? 1 : i));
				reflect(word, middle, 0);
				return word;
			}
		} else {
			int i = findIndexToCompare(word, middle, 1);
			if (word[middle + i + 1] >= word[middle - i - 1]) {
				inc(word, middle);
				reflect(word, middle, 1);
				return word;
			} else {
				reflect(word, middle, 1);
				return word;
			}
		}
	}

	private static int findIndexToCompare(final int[] word, int middle,
			int offeset) {
		int i = 0;
		while ((middle != i)
				&& word[middle + i + offeset] == word[middle - 1 - i]) {
			i++;
		}

		return middle == i ? 0 : i;

	}

	private static void reflect(int[] word, int middle, int offset) {
		for (int i = 0; i < (middle + offset); ++i) {
			word[word.length - 1 - i] = word[i];
		}
	}

	private static void inc(int[] word, int startIndex) {
		int digit = word[startIndex];
		word[startIndex] = (digit + 1) % 10;
		int carry = (digit + 1) / 10;
		for (int i = startIndex - 1; i >= 0 || carry != 0; i--) {
			digit = (i < 0 ? 0 : word[i]) + carry;
			word[i] = (digit > 9) ? digit % 10 : digit;
			carry = digit / 10;
		}

	}

	private static boolean areAll9s(int[] num) {
		int size = num.length;
		for (int i = 0; i < size; ++i)
			if (num[i] != 9)
				return false;
		return true;
	}

}
