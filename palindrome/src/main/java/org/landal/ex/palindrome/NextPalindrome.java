package org.landal.ex.palindrome;

public class NextPalindrome {

	public String findNext(String num) {

		String res = null;
		for (long i = new Long(num) + 1;; i++) {
			res = Long.toString(i);
			if (isPalindrome(res.toCharArray()))
				return res;
		}

	}

	private boolean isPalindrome(char[] cs) {
		for (int i = 0; i < cs.length / 2; i++) {
			if(cs[i] != cs[cs.length - i - 1]){
				return false;
			}
		}	
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(new NextPalindrome().findNext("1222"));
		System.out.println(new NextPalindrome().findNext("3210134"));
		System.out.println(new NextPalindrome().findNext("4530324"));
		System.out.println(new NextPalindrome().findNext("4530224"));
		System.out.println(new NextPalindrome().findNext("2222"));
		System.out.println(new NextPalindrome().findNext("4530352"));
		System.out.println(new NextPalindrome().findNext("1991"));
		
	}

}
