package org.landal.ex.palindrome;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			int numberOfLinesToRead = new Integer(br.readLine());
			for (int i = 0; i < numberOfLinesToRead; i++) {
				System.out.println(Palindrome.findNext(br.readLine().trim()));
			}
		} catch (Exception e) {

		}
	}

}

class Palindrome {

	public static String findNext(String word) {

		word = removeTrailingZero(word);

		int middle = word.length() / 2;

		if (word.length() % 2 == 0) {
			int i = findIndexToCompare(word, middle, 0);
			if (Integer.valueOf(word.substring(middle + i, middle + i + 1)) < Integer
					.valueOf(word.substring(middle - 1 - i, middle - i))) {
				String midLeft = word.substring(0, middle);
				return midLeft + reverse(midLeft);
			} else {
				String midLeft = inc(word.substring(0, middle));
				if (midLeft.length() != word.substring(0, middle).length()) {
					return midLeft
							+ reverse(midLeft
									.substring(0, midLeft.length() - 1));
				} else {
					return midLeft + reverse(midLeft);
				}

			}
		} else {
			int i = findIndexToCompare(word, middle, 1);
			if (Integer.valueOf(String.valueOf(word.charAt(middle + i + 1))) >= Integer
					.valueOf(String.valueOf(word.charAt(middle - i - 1)))) {

				String midLeft = inc(word.substring(0, middle + 1));
				if (midLeft.length() != word.substring(0, middle + 1).length()) {
					return midLeft
							+ reverse(midLeft
									.substring(0, midLeft.length() - 2));
				} else {
					return midLeft
							+ reverse(midLeft
									.substring(0, midLeft.length() - 1));
				}

			} else {
				String midLeft = word.substring(0, middle);
				return midLeft + word.charAt(middle) + reverse(midLeft);
			}
		}
	}

	private static String removeTrailingZero(String word) {

		int i = 0;
		while (word.charAt(i) == '0') {
			++i;
		}

		return i > 0 ? word.substring(i) : word;
	}

	private static int findIndexToCompare(final String word, int middle,
			int offeset) {
		int i = 0;
		while ((middle != i)
				&& Integer.valueOf(word.substring(middle + i + offeset, middle
						+ i + 1 + offeset)) == Integer.valueOf(word.substring(
						middle - 1 - i, middle - i))) {
			i++;
		}

		return i == middle ? 0 : i;
	}

	private static String reverse(String str) {
		StringBuilder sb = new StringBuilder();
		for (int i = str.length() - 1; i >= 0; i--) {
			sb.append(str.charAt(i));
		}

		return sb.toString();
	}

	public static String inc(String number) {
		StringBuilder buf = new StringBuilder();
		int digit = Integer.parseInt(Character.toString(number.charAt(number
				.length() - 1)));
		buf.append((digit + 1) % 10);
		int carry = (digit + 1) / 10;
		for (int i1 = number.length() - 2; i1 >= 0 || carry != 0; i1--) {
			digit = (i1 < 0 ? 0 : Integer.parseInt(Character.toString(number
					.charAt(i1)))) + carry;
			buf.append((digit > 9) ? digit % 10 : digit);
			carry = digit / 10;
		}
		return buf.reverse().toString();
	}

}
