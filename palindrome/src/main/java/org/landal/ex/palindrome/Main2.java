package org.landal.ex.palindrome;

public class Main2 {

}

class PalindromeFinder {

	public static String findNext(String num) {
		int[] numArray = new int[num.length()];
		for (int i = 0; i < num.length(); i++) {
			numArray[i] = Integer.parseInt(Character.toString(num.charAt(i)));
		}
		return findNextPalindrome(numArray);
	}

	public static String findNextPalindrome(int[] num) {
		if (areAll9s(num)) {
			StringBuilder sb = new StringBuilder("1");
			int size = num.length;
			for (int i = 1; i < size; i++)
				sb.append("0");
			sb.append("1");
			return sb.toString();
		}

		else {
			return toString(generateNextPalindromeUtil(num));

		}
	}

	private static String toString(int[] result) {
		StringBuilder sb = new StringBuilder();
		for (int i = result.length - 1; i >= 0 ; --i) {
			sb.append(result[i]);
		}
		return sb.toString();
	}

	private static int[] generateNextPalindromeUtil(int[] num) {
		int size = num.length;
		int mid = size / 2;
		boolean leftsmaller = false;
		int i = mid - 1;
		int j = (size % 2 != 0) ? mid + 1 : mid;
		while (i > 0 && num[i] == num[j]) {
			i--;
			j++;
		}

		if (i >= 0 || num[i] < num[j])
			leftsmaller = true;

		while (i >= 0) {
			num[j] = num[i];
			j++;
			i--;
		}

		if (leftsmaller == true) {
			int carry = 1;
			i = mid - 1;

			if (size % 2 == 1) {
				num[mid] += carry;
				carry = num[mid] / 10;
				num[mid] = num[mid] % 10;
				j = mid + 1;
			} else
				j = mid;

			while (i >= 0) {
				num[i] += carry;
				carry = num[i] / 10;
				num[i] = num[i] % 10;
				num[j++] = num[i--];
			}

		}

		return num;
	}

	private static boolean areAll9s(int[] num) {
		int size = num.length;
		for (int i = 0; i < size; ++i)
			if (num[i] != 9)
				return false;
		return true;
	}

}
