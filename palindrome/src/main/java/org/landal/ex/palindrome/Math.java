package org.landal.ex.palindrome;

public class Math {
	
	public static String add(String addend1, String addend2) {
	    StringBuilder buf = new StringBuilder();
	    int digit1 = 0;
	    int digit2 = 0;
	    int digit = 0;
	    for ( int i1 = addend1.length() - 1, i2 = addend2.length() - 1, carry = 0;
	          i1 >= 0 || i2 >= 0 || carry != 0;
	          i1--, i2-- ) {
	        digit1 = i1 < 0 ? 0 :
	                     Integer.parseInt(Character.toString(addend1.charAt(i1)));
	        digit2 = i2 < 0 ? 0 :
	                     Integer.parseInt(Character.toString(addend2.charAt(i2)));

	        digit = digit1 + digit2 + carry;
	        if (digit > 9) {
	            carry = 1;
	            digit -= 10;
	        } else {
	            carry = 0;
	        }

	        buf.append(digit);
	    }
	    return buf.reverse().toString();
	}

	public static String inc(String number) {
		StringBuilder buf = new StringBuilder();
		int digit = Integer.parseInt(Character.toString(number.charAt(number
				.length() - 1)));
		buf.append((digit + 1) % 10);
		int carry = (digit + 1) / 10;
		for (int i1 = number.length() - 2; i1 >= 0 || carry != 0; i1--) {
			digit = (i1 < 0 ? 0 : Integer.parseInt(Character.toString(number
					.charAt(i1)))) + carry;			
			buf.append((digit > 9) ? digit % 10 : digit);
			carry = digit / 10;
		}
		return buf.reverse().toString();
	}

}
