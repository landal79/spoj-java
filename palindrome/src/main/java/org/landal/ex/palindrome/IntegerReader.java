package org.landal.ex.palindrome;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class IntegerReader {

	public static void main(String[] args) {		
		try (Scanner scanner = new Scanner(System.in)) {
			
			int count = Integer.valueOf(scanner.nextLine());	
			List<int[]> reads = new ArrayList<>();	
			for(int i = 0; i < count; ++i){
				reads.add(toIntArray(scanner.nextLine().toCharArray()));
			}
			printListOfArrays(reads);
		}

	}

	private static void printListOfArrays(List<int[]> reads) {
		
		for (int[] is : reads) {
			System.out.println(Arrays.toString(is));
		}
		
	}

	private static int[] toIntArray(char[] cs) {
		int[] result = new int[cs.length];
		for (int i = 0; i < cs.length; ++i) {
			result[i] = new Integer(Character.toString(cs[i]));
		}
		return result;
	}

}
